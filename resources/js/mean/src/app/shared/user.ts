export class User {
   _id: number;
   email: String;
   password: String;
   firstName: String;
   lastName: String;
   token?: String;
}
