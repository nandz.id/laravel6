import { Component, ViewChild, HostListener, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { MatSidenav } from '@angular/material/sidenav';
import { ApiService } from './shared/api.service';
import { User } from './shared/user';
import { MatSnackBar } from '@angular/material/snack-bar';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})

export class AppComponent implements OnInit {

  currentUser: User;

  opened = true;
  @ViewChild('sidenav', { static: true }) sidenav: MatSidenav;

  constructor( private router: Router, private authenticationService: ApiService, private snackBar: MatSnackBar) {
    this.authenticationService.currentUser.subscribe( x => this.currentUser = x);
  }

  ngOnInit() {
    console.log(window.innerWidth);
    if (window.innerWidth < 768) {
      this.sidenav.fixedTopGap = 55;
      this.opened = false;
    } else {
      this.sidenav.fixedTopGap = 55;
      this.opened = true;
    }
  }

  @HostListener('window:resize', ['$event'])
  onResize(event) {
    if (event.target.innerWidth < 768) {
      this.sidenav.fixedTopGap = 55;
      this.opened = false;
    } else {
      this.sidenav.fixedTopGap = 55;
      this.opened = true;
    }
  }

  isBiggerScreen() {
    const width = window.innerWidth || document.documentElement.clientWidth || document.body.clientWidth;
    if (width < 768) {
      return true;
    } else {
      return false;
    }
  }

  logout() {
    this.authenticationService.logout();
    this.snackBar.open('Anda telah logout', 'Notice', {
      duration: 2000,
      verticalPosition: 'top',
      // panelClass: ['success-snackbar']
    });
    this.router.navigate(['/login']);
  }
}
