import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { first } from 'rxjs/operators';
// import swal from 'sweetalert';

// SERVICES
import { ApiService } from '../../shared/api.service';
import { from } from 'rxjs';
import { MatSnackBar } from '@angular/material/snack-bar';

// ERROR MESSAGE

@Component({
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.css'],
    providers: [ ]
})

export class LoginComponent implements OnInit {

    loginForm: FormGroup;
    loading = false;
    submitted = false;
    returnUrl: string;
    error = '';

    constructor(
        private formBuilder: FormBuilder,
        private route: ActivatedRoute,
        private router: Router,
        private authenticationService: ApiService,
        private snackBar: MatSnackBar
    ) {
      if (this.authenticationService.currentUserValue) {
        this.router.navigate([['/']]);
      }
    }

    ngOnInit() {
      this.loginForm = this.formBuilder.group({
        email: ['', Validators.required],
        password: ['', Validators.required]
      });
      this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/';
    }

    get f() {
      return this.loginForm.controls;
    }

    onSubmit() {
      // swal('Your imaginary file is safe!');
      this.submitted = true;

      if (this.loginForm.invalid) {
        return;
      }
      this.loading = true;

      this.authenticationService.login(this.f.email.value, this.f.password.value)
        .pipe(first())
          .subscribe(
            data => {
              this.snackBar.open('Login Success !', 'Success', {
                duration: 2000,
                verticalPosition: 'top',
                // panelClass: ['success-snackbar']
              });
              this.router.navigate([this.returnUrl]);
            },
            error => {
              this.snackBar.open('Login Gagal !', 'Alert', {
                duration: 2000,
                verticalPosition: 'top',
                // panelClass: ['danger-snackbar']
              });
              if (error === 'Bad Request') {
                error = 'Username / Password Invalid';
              }
              this.error = error;
              this.loading = false;
            });

    }
}
