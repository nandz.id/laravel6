import { Student } from './../../shared/student';
import { ApiService } from './../../shared/api.service';
import { Component, ViewChild, OnInit, AfterViewInit } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource, MatSort } from '@angular/material';
import { MatSnackBar } from '@angular/material/snack-bar';

@Component({
  selector: 'app-students-list',
  templateUrl: './students-list.component.html',
  styleUrls: ['./students-list.component.css']
})

export class StudentsListComponent implements OnInit, AfterViewInit {
  StudentData: any = [];
  dataSource: MatTableDataSource<Student>;
  @ViewChild(MatPaginator, { static: false }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: false }) sort: MatSort;
  displayedColumns: string[] = ['_id', 'student_name', 'student_email', 'section', 'action'];

  constructor(private studentApi: ApiService, private snackBar: MatSnackBar) {

  }

  ngOnInit() {
    this.getStudentList();
   }

  ngAfterViewInit() {
    // this.studentApi.GetStudents().subscribe(
    //   data => {
    //     this.dataSource = new MatTableDataSource(data);
    //     this.dataSource.sort = this.sort;
    //   }
    // );
  }

  getStudentList = () =>  this.studentApi.GetStudents().subscribe(res => {
    this.StudentData = res;
    this.dataSource = new MatTableDataSource<Student>(this.StudentData);
    setTimeout(() => {
      this.dataSource.sort = this.sort;
      this.dataSource.paginator = this.paginator;
    }, 0);
  })

  deleteStudent(index: number, e) {
    if (window.confirm('Are you sure')) {
      const data = this.dataSource.data;
      data.splice((this.paginator.pageIndex * this.paginator.pageSize) + index, 1);
      this.dataSource.data = data;
      this.studentApi.DeleteStudent(e._id).subscribe();
      this.snackBar.open(`Delete ${e._id} Student Success !`, 'Success', {
        duration: 2000,
        verticalPosition: 'top',
        // panelClass: ['success-snackbar']
      });
    }
  }

  applyFilter(filterValue: string) {
    filterValue = filterValue.trim();
    filterValue = filterValue.toLowerCase();
    this.dataSource.filter = filterValue;
  }

  trackByUid(index, item) {
    return item.uid;
  }

}
