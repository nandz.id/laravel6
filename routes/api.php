<?php



/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });
    Route::post('login', 'APIController@login');
    Route::post('register', 'APIController@register');
    Route::get('profile', 'APIController@getAuthenticatedUser');
    Route::group(['middleware' => 'jwt.verify'], function() {
        Route::post('add-student', 'StudentController@storeStudent');
        Route::get('read-student/{id}', 'StudentController@detailStudent');
        Route::put('update-student/{id}', 'StudentController@updateStudent');
        Route::delete('delete-student/{id}', 'StudentController@deleteStudent');
        Route::get('get-student', 'StudentController@getStudent');
    });



