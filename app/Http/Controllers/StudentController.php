<?php

namespace App\Http\Controllers;
use App\Student;
use Illuminate\Http\Request;

class StudentController extends Controller
{

   public function storeStudent(Request $request) {

        $data = new Student;
        $data->student_name = $request->input('student_name');
        $data->student_email = $request->input('student_email');
        $data->section = $request->input('section');
        $data->subjects = $request->input('subjects');
        $data->dob = $request->input('dob');
        $data->gender = $request->input('gender');
        $data->save();

        return response()->json(['msg' => 'data sukses di simpan ke databse', 'status' => 200], 200);

   }

   public function getStudent() {
        $data = Student::all();

        if($data->count() > 0) {
           return response()->json($data, 200);
        }
        return response()->json(['data' => 'no data on table', 'status' => 400], 400);
   }

   public function detailStudent($id) {
        $data = Student::find($id);
        if(!empty($data->count())) {
            return response()->json($data, 200);
        }
        return response()->json(['msg' => 'data not found'], 200);
   }

   public function updateStudent($id, Request $request) {

        $data = Student::where('_id',$id);
        if(!empty($data->count())) {
            $data->update([
                'student_name' => $request->input('student_name'),
                'student_email' => $request->input('student_email'),
                'section' => $request->input('section'),
                'subjects' => $request->input('subjects'),
                'dob' => $request->input('dob'),
                'gender' => $request->input('gender')
            ]);
            return response()->json(['msg' => 'data sukses terupdate !'], 200);
        }
        return response()->json(['msg' => 'error, id not found on database'], 400);
   }

   public function deleteStudent($id) {
        $data = Student::find($id);
        if(!empty($data->count())) {
            $data->delete();
            return response()->json(['msg' => 'data sukses dihapus !'], 200);
        }
        return response()->json(['msg' => 'data gagal dihapus'], 400);
   }

}
